function problem1(users) {

    let errorMessage;
    if(!users) {
        errorMessage = "Data is not present";
        return errorMessage;
    }

    let usersInterestedInPlayingGames = [];
    for(let user in users) {
      if(users[user].interests[0].includes("Video Games")) {
        usersInterestedInPlayingGames.push(users[user]);
      }
    }

    return usersInterestedInPlayingGames;
}

module.exports = problem1;