function problem4(users) {

    let errorMessage;
    if(!users) {
        errorMessage = "Data is not present";
        return errorMessage;
    }


    let developersByLanguage = { pythonDevelopers: [], javascriptDevelopers: [], golangDevelopers: []};
    let pythonDevelopers = [];
    let javascriptDevelopers = [];
    let golangDevelopers = [];

    for(let user in users) {
      if(users[user].designation.toLowerCase().includes("python")) {
        pythonDevelopers.push(users[user]);
      }

      if(users[user].designation.toLowerCase().includes("javascript")) {
        javascriptDevelopers.push(users[user]);
      }

      if(users[user].designation.toLowerCase().includes("golang")) {
        golangDevelopers.push(users[user]);
      }
      
    }
    developersByLanguage.pythonDevelopers = pythonDevelopers;
    developersByLanguage.javascriptDevelopers = javascriptDevelopers;
    developersByLanguage.golangDevelopers = golangDevelopers;
    
    return developersByLanguage;
}

module.exports = problem4;