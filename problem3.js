function problem3(users) {

    let errorMessage;
    if(!users) {
        errorMessage = "Data is not present";
        return errorMessage;
    }

    let usersWithMasterDegree = [];
    for(let user in users) {
        if(users[user].qualification === "Masters") {
            usersWithMasterDegree.push(users[user]);
        }
    }

    return usersWithMasterDegree;
}

module.exports = problem3;