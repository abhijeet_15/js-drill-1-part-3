function problem2(users) {

    let errorMessage;
    if(!users) {
        errorMessage = "Data is not present";
        return errorMessage;
    }

    let usersInGermany = [];
    for(let user in users) {
        if(users[user].nationality === "Germany") {
            usersInGermany.push(users[user]);
        }
    }

    return usersInGermany;
}

module.exports = problem2;